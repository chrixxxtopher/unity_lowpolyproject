using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets.Characters.ThirdPerson
{
[RequireComponent(typeof (ThirdPersonCharacter))]
public class ThirdPersonUserControl : MonoBehaviour
{
    private ThirdPersonCharacter m_Character; // A reference to the ThirdPersonCharacter on the object
    private Transform m_Cam;                  // A reference to the main camera in the scenes transform
    private Vector3 m_CamForward;             // The current forward direction of the camera
    private Vector3 m_Move;
    private bool m_Jump;                      // the world-relative desired move direction, calculated from the camForward and user input.
    private bool m_AutoRunPressed;
    private bool m_isAutoRunning;
   //public Inventory inventory;
    private void Start()
    {
        if (Camera.main != null)
        {
            m_Cam = Camera.main.transform;
        }
        else
        {
            Debug.LogWarning(
                "Warning: no main camera found. Third person character needs a Camera tagged \"MainCamera\", for camera-relative controls.", gameObject);
            // we use self-relative controls in this case, which probably isn't what the user wants, but hey, we warned them!
        }
        m_Character = GetComponent<ThirdPersonCharacter>();
    }


    private void Update()
    {
        if (!m_Jump)
        {
            m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");

        }
        if (m_AutoRunPressed = CrossPlatformInputManager.GetButtonDown("AutoRun"))
        {
            m_isAutoRunning = !m_isAutoRunning;
        }
    }

    // Fixed update is called in sync with physics
    private void FixedUpdate()
    {

        float h = CrossPlatformInputManager.GetAxis("Horizontal");
        float v = CrossPlatformInputManager.GetAxis("Vertical");
        bool crouch = Input.GetKey(KeyCode.C);


        // calculate move direction to pass to character
        if (m_Cam != null)
        {
            // calculate camera relative direction to move:
            m_CamForward = Vector3.Scale(m_Cam.forward, new Vector3(1, 0, 1)).normalized;
            m_Move = v * m_CamForward + h * m_Cam.right;
        }
        else
        {
            // we use world-relative directions in the case of no main camera
            m_Move = v * Vector3.forward + h * Vector3.right;
            Debug.Log("Shouldn't be here...no main camera??");
        }
        // L_SHIFT to sprint
        if (Input.GetKey(KeyCode.LeftShift)) m_Move *= 0.5f;

        //if autoRun is true AND theres no input coming from the left stick/wsad/keys
        if (m_isAutoRunning && Math.Abs(CrossPlatformInputManager.GetAxis("Horizontal"))
            + Math.Abs(CrossPlatformInputManager.GetAxis("Vertical")) < 2 * float.Epsilon)
        {
            //pass constant forward direction to the move function
            Vector3 forward = new Vector3(0, 0, 1);
            m_Character.Move(forward, crouch, m_Jump);
        }
        else
        {
            //!!!!!!MOVE THE CHARACTER!!!!!!
            m_Character.Move(m_Move, crouch, m_Jump);
            m_Jump = false;
        }
    }
    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        //IIventoryItem item = hit.collider.GetComponent<IInventoryItem>();
        //player checks if the thing he collided with is an item
//        if (item != null)
        {
           // inventory.AddItem(item);
        }
    }
}
}