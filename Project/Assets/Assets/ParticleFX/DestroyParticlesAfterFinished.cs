﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// This class destroys a particle system after its last particle is gone.
public class DestroyParticlesAfterFinished : MonoBehaviour 
{
	void Start () 
    {
        Destroy(gameObject, GetComponent<ParticleSystem>().main.duration
                + GetComponent<ParticleSystem>().main.startLifetime.constantMax);
	}

}
