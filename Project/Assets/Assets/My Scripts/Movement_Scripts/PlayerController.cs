﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityStandardAssets.CrossPlatformInput;

namespace LowPoly.Character
{
    [RequireComponent(typeof(CustomMovement))]
    public class PlayerController : MonoBehaviour
    {
        private CustomMovement m_Character;
        private Transform m_Cam;
        private Vector3 m_CamForward;       // The current forward direction of the camera
        private Vector3 m_Move;
        private bool m_Jump;
        private bool m_AutoRunPressed;
        private bool m_isAutoRunning;
        public InventoryList inventory;
        public GameObject m_hand;
        public HUD hud;
        Animator animator;
        SpecialAbilities m_SpecialAbilities;
        public StaminaSystem m_StaminaSystem;
        private int startHealth;
        [SerializeField] InventoryItemBase mItemIAmHolding = null;
        public InventoryItemBase mItemRequestingToBeCollected;
        private bool alreadySetSpeedMultiplier = false;


        private void Start()
        {
            if (Camera.main != null)
            {
                m_Cam = Camera.main.transform;
            }
            else
            {
                Debug.LogWarning("No camera tagged as 'MainCamera'.");
            }
            m_Character = GetComponent<CustomMovement>(); //be able to move
            animator = GetComponent<Animator>(); //pick stuff up & drop stuff
            m_SpecialAbilities = GetComponent<SpecialAbilities>(); //cast spells

            //Subscribe to these events
            inventory.OnItemUsed += Inventory_ItemUsed;
            inventory.OnItemRemoved += Inventory_ItemRemoved;
        }


        private void Update()
        {
            if (!m_Jump && CrossPlatformInputManager.GetButtonDown("Jump")) //If we are not currently performing a jump...
            {
                //m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
                if(m_StaminaSystem.AttemptJump())
                {
                    m_Jump = true;
                }
                else
                {
                    m_Jump = false;
                }
            }

            if (m_AutoRunPressed = CrossPlatformInputManager.GetButtonDown("AutoRun"))
            {
                m_isAutoRunning = !m_isAutoRunning;
            }

            if (CrossPlatformInputManager.GetButtonDown("Interact"))
            {
                if (mItemRequestingToBeCollected != null)
                {
                    animator.SetTrigger("Pickup");
                    mItemRequestingToBeCollected.GetDetailsAboutItem();
                    inventory.AddItem(mItemRequestingToBeCollected);
                    //OnPickup() destroys rigidbody
                    mItemRequestingToBeCollected.OnPickup();

                    if (hud.IsMessagePanelOpened)
                    {
                        hud.CloseMessagePanel();
                    }
                    mItemRequestingToBeCollected = null;
                }
                else
                {
                    Debug.Log("item is null or not able to be collected");
                }
            }
            if(CrossPlatformInputManager.GetButtonDown("SelfHeal"))
            {
                m_SpecialAbilities.AttemptSpecialAbility(0,this.gameObject);
            }
            if (CrossPlatformInputManager.GetButtonDown("AOE"))
            {
                m_SpecialAbilities.AttemptSpecialAbility(1, this.gameObject);
            }
        }
        private void FixedUpdate()
        {
            float h = CrossPlatformInputManager.GetAxis("Horizontal");
            float v = CrossPlatformInputManager.GetAxis("Vertical");


            // calculate move direction to pass to character
            if (m_Cam != null)
            {
                // calculate camera relative direction to move:
                m_CamForward = Vector3.Scale(m_Cam.forward, new Vector3(1, 0, 1)).normalized;
                m_Move = v * m_CamForward + h * m_Cam.right;
            }
          
            //TODO: sync footsteps with sprinting
            if (Input.GetKey(KeyCode.LeftShift)) //sprint costs 1stamina per 0.25seconds
            {
                if (m_StaminaSystem.AttemptSprint())
                {
                    m_Character.m_MoveSpeedMultiplier = 1.5f;
                    m_Character.m_AnimSpeedMultiplier = 1.5f;
                }
            }
            else
            {
                m_Character.m_MoveSpeedMultiplier = 1f;
                m_Character.m_AnimSpeedMultiplier = 1f;
            }


            /// <summary>
            /// Vector3.forward and the others are in WORLD SPACE, 
            /// so they will always move you in the same direction -  
            /// no matter which direction your character is in.
            /// 
            /// To FIX THIS you can replace the Vector3.forward with transform.forward. 
            /// transform.forward is the forward direction of the transform, 
            /// taking rotation into account.
            /// </summary>


            //if autoRun is true AND theres no input coming from the left stick/wsad/keys
            if (m_isAutoRunning && Math.Abs(CrossPlatformInputManager.GetAxis("Horizontal"))
                + Math.Abs(CrossPlatformInputManager.GetAxis("Vertical")) < 2 * float.Epsilon)
            {
                //pass constant forward direction to the move function WITH transform's rotation in mind
                Vector3 forward = transform.forward;
                m_Character.Move(forward, m_Jump);
            }
            else
            {
                //!!!!!!MOVE THE CHARACTER!!!!!!
                m_Character.Move(m_Move, m_Jump);
                m_Jump = false;
            }

            if (mItemIAmHolding != null && CrossPlatformInputManager.GetButtonDown("DropItem"))
            {
                DropCurrentItem();
            }

        }


        public bool IsArmed
        {
            get
            {
                if(mItemIAmHolding != null)
                {
                    return mItemIAmHolding.ItemType == ItemType.Weapon;
                }
                return false;
                
            }
        }
        private void Inventory_ItemRemoved(object sender, InventoryEventArgs e)
        {
            InventoryItemBase itemBase = e.m_Item;

            GameObject goItem = (itemBase as MonoBehaviour).gameObject;
            goItem.SetActive(true);
            goItem.transform.parent = null;
        }

        /*If we are here, our inventory secretary has informed us that 
        *there has been a request to use an item (object sender).
        *We want to do something with the item here..
        */
        private void Inventory_ItemUsed(object sender, InventoryEventArgs e)
        {
            if(e.m_Item.ItemType != ItemType.Consumable)//dont carry food
            {
                if(mItemIAmHolding != null)//if im holding an item already
                {
                    Debug.Log("Item Iam holding is not null aka im holding an item");
                    SetItemActive(mItemIAmHolding, false);//deactivate it
                }
            }

            InventoryItemBase itemTemp = e.m_Item; //get the new item 
            mItemIAmHolding = e.m_Item;
            SetItemActive(mItemIAmHolding, true); //activate this one


        }
        private void SetItemActive(InventoryItemBase item, bool active)
        {
            GameObject currentItem = (item as MonoBehaviour).gameObject;
            currentItem.SetActive(active);
            currentItem.transform.parent = active ? m_hand.transform : null;
        }

        private void DropCurrentItem()
        {
            GameObject itemToDrop = mItemIAmHolding.gameObject;

            inventory.RemoveItem(mItemIAmHolding);

            Rigidbody rbItem = itemToDrop.AddComponent<Rigidbody>();
            if (rbItem != null)
            {
                animator.SetTrigger("DropItem");   

                rbItem.AddForce(transform.up * 2.0f, ForceMode.Impulse);

                //we need a delay so the item doesn't just hang in the air
                Invoke("DestroyDropItem", 0.6f);
            }
        }
        public void DestroyDropItem()
        {
            Destroy((mItemIAmHolding as MonoBehaviour).GetComponent<Rigidbody>());
            mItemIAmHolding = null;
        }
    }
}

//i am just testing this line with sourcetree