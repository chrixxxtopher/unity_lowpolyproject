﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace LowPoly.Character
{
    public class CritterAI : MonoBehaviour
    {
        Animator m_animator;
        NavMeshAgent m_Agent;
        bool m_Isdead = false;
        int layerMask = -1;
        public GameObject player;
        public float runAwayDistance = 4f;
        public float wanderRadius = 20f;
        public float wanderTimer = 3f;
        private float timer;
        public bool isWanderingAround = true;
        public GameObject[] itemsDeadState = null;
        [SerializeField] AudioClip[] animalSounds;
        AudioClip runAwaySound;
        AudioSource m_audioSource;

        private void OnEnable()
        {
            timer = wanderTimer;
        }
        private void Start()
        {
            player = GameObject.FindWithTag("Player");
            m_audioSource = GetComponent<AudioSource>();
            m_Agent = GetComponent<NavMeshAgent>();
            m_animator = GetComponent<Animator>();
        }

        private bool IsNavMeshMoving
        {
            get
            {
                return m_Agent.velocity.magnitude > 0.1f;
            }
        }
        //This is for an enemy attacking me!
        private void OnCollisionEnter(Collision collision)
        {
            //weapon item
            InventoryItemBase item = collision.collider.gameObject.GetComponent<InventoryItemBase>();
            if(item != null)
            {
                //i was hit by a weapon
            }
        }

        void Update()
        {
            if(IsNavMeshMoving)
            {
                m_animator.SetBool("walk", true);
            }
            else
            {
                m_animator.SetBool("walk", false);
            }
            timer += Time.deltaTime;
            //Calculate our distance from the player every frame
            float distance = Vector3.Distance(transform.position, player.transform.position);
            if(distance < runAwayDistance)
            {
                isWanderingAround = false;
                Vector3 dirToPlayer = transform.position - player.transform.position;
                Vector3 newPositionToRunTo = transform.position + dirToPlayer;

                m_Agent.SetDestination(newPositionToRunTo);
                m_animator.SetBool("walk", true);
            }
            else
            {
                isWanderingAround = true;
            }
            if(isWanderingAround && timer >= wanderTimer)
            {
                //Debug.Log("time.deltaTime timer is greater than or equal to wander timer");
                WanderAround(transform.position);
                timer = 0;
            }
        }

        public void WanderAround(Vector3 origin)
        {
            NavMeshHit navMeshHit;
            Vector3 randomDirection = Random.insideUnitSphere * wanderRadius;
            randomDirection += origin;
            StartCoroutine(MakeNoises());
            NavMesh.SamplePosition(randomDirection, out navMeshHit, wanderRadius, layerMask);

            m_Agent.SetDestination(navMeshHit.position);
        }

        public void MakeAnimalNoises()
        {
            m_audioSource.clip = animalSounds[Random.Range(0, animalSounds.Length)];
            m_audioSource.volume = 0.2f;
            m_audioSource.Play();
            StartCoroutine(MakeNoises());
        }

        IEnumerator MakeNoises()
        {
            float random = Random.Range(5, 10);
            yield return new WaitForSeconds(random);
            MakeAnimalNoises();
        }
    }
}

//sheep is rvineyard run away sound