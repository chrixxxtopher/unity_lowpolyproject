﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//TODO: Lock x axis rotation
public class FaceWhoIsTargettingMe : MonoBehaviour 
{
    public Transform target;


    private void FixedUpdate()
    {
        Vector3 targetPosition = new Vector3(target.position.x,
                                                 this.transform.position.y,
                                                 target.position.z);
        this.transform.LookAt(targetPosition);
    }

}
