﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Levitate : MonoBehaviour 
{
    Vector3 levitate;
    Vector3 tempLevitate;
    public float frequency = 1f;
    public float amplitude = 0.5f;

	void Start () 
    {
        levitate = transform.position;
	}
	
	// Update is called once per frame
	void Update () 
    {
        tempLevitate = levitate;
        tempLevitate.y += Mathf.Sin(Time.fixedTime * Mathf.PI * frequency) * amplitude;

        transform.position = tempLevitate;
	}

}
