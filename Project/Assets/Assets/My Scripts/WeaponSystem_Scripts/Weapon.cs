﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LowPoly.Character;

namespace LowPoly.Weapon
{
    public class Weapon : InventoryItemBase //better to inherit from this than monobehavior 
    {
        [SerializeField] PlayerController m_Player;
        WeaponSystem m_Player_WeaponSystem;
        [SerializeField] GameObject weaponPrefab;
        [SerializeField] AnimationClip attackAnimation;
        [SerializeField] float meleeAttackRadius = 2f; //unarmed punching/kicking melee radius
        [SerializeField] float rangedAttackRadius = 0f; //assumed no ranged weapons
        [SerializeField] int[] variableMeleeDamage = { 0, 3 }; //default punching/kicking damage
        private int meleeDamage = 1; //the meleeDamage the player gets (actual damage after picking variable damage)
                                     //you can't attack faster than the below value
        [SerializeField] float minimumTimeBetweenHits = 1f;
        [SerializeField] float damageDelay = .5f;
        [SerializeField] float criticalMultiplier = 1.75f; //1 * 1.75 = totalDamage
        private bool hasCollided = false;
        public SphereCollider m_HitCollider;

        public SphereCollider GetHitCollider()
        {
            return m_HitCollider;
        }

        public float GetDamageDelay()
        {
            return damageDelay;
        }
        public int GetMeleeDamage()
        {
            SetMeleeDamage();
            return meleeDamage;
        }
        public void SetMeleeDamage()//add 1 to the value of [1] bc exclusive
        {
            this.meleeDamage = Random.Range(variableMeleeDamage[0], (variableMeleeDamage[1]) + 1);
        }
        public float GetMeleeAttackRadius()
        {
            return meleeAttackRadius;
        }
        public float GetRangedAttackRadius()
        {
            return rangedAttackRadius;
        }
        public float GetCriticalMultiplier()
        {
            return criticalMultiplier;
        }
        public float GetMinTimeBetweenHits()
        {
            return minimumTimeBetweenHits;
        }
        public GameObject GetWeaponPrefab()
        {
            return weaponPrefab;
        }
        public AnimationClip GetAttackAnimationClip()
        {
            attackAnimation.events = new AnimationEvent[0]; //clears animation event list to disable hits bc idk what to do?
            return attackAnimation;
        }

        public override void OnPickup()
        {
            m_HitCollider = GetComponent<SphereCollider>();
            base.OnPickup();
            m_Player_WeaponSystem = m_Player.GetComponent<WeaponSystem>();
            m_Player_WeaponSystem.SetCurrentWeapon(this);
            m_Player_WeaponSystem.OverrideAnimatorController();
        }

        private void OnTriggerEnter(Collider other)
        {
            //oh NO - a string LITERAL...the know it all's are going to go insane!!
            if (other.name == "Liam" && this.isActiveAndEnabled && this != null)
            {
                m_Player = other.GetComponent<PlayerController>();
                hud.OpenMessagePanel(this);
                m_Player.mItemRequestingToBeCollected = this;
            }
            //an attacker entered our trigger (we swing our weapon and an attacker enters)
            if(other.tag == "Attackable" && m_Player.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Attack"))
            {
                if (!m_Player_WeaponSystem.isAttacking)
                {
                    Debug.Log("called attack coroutine to start");
                    m_Player_WeaponSystem.TestAttackTarget(other.gameObject);
                }
            }
        }

        private void OnTriggerExit(Collider other)
        {
            hud.CloseMessagePanel();
        }
    }
}