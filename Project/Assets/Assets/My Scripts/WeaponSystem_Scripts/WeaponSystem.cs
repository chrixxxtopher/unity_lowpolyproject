﻿
using UnityEngine;
using System;
using System.Collections;
using LowPoly.Character;
using UnityStandardAssets.CrossPlatformInput;

//todo...seperate weaponsystem and combat system...fuck me i knew it
//bc all characters that can fight should use the same fighting script
namespace LowPoly.Weapon
{
    public class WeaponSystem : MonoBehaviour
    {
        [SerializeField] Weapon m_WeaponInUse;
        //GameObject weaponObject; //To instantiate a prefab of the weapon
        [SerializeField] AnimatorOverrideController animatorOverrideController;
        Animator animator;
        CustomMovement m_CustomMovement;

        GameObject currentTarget;
        const string ATTACK_TRIGGER = "Attack";
        const string DEFAULT_ATTACK = "DEFAULT_ATTACK";
        float lastHitTime;
        bool hasTarget = false;
        public bool isAttacking = false;
        [Range(.1f, 1.0f)] [SerializeField] float criticalHitChance = 0.1f;//10% chance

        private void Start()
        {
            if (m_WeaponInUse != null)
            {
                OverrideAnimatorController();
            }
        }

        private void Update()//target COULD die at any frame
        {
            ////check if target can be attacked
            //bool targetIsDead = false;
            //bool targetIsOutOfRange = false;
            //if (targetIsOutOfRange || targetIsDead))
            //{
            //    StopAllCoroutines();
            //}
            if (CrossPlatformInputManager.GetButtonDown("Attack"))
            {
                animator.SetBool("WaitingToAttack", true);
                animator.SetTrigger("Attack");

                if(!hasTarget)
                {
                    animator.SetBool("WaitingToAttack", false);
                }
            }
        }

        public Weapon GetCurrentWeapon()
        {
            //this needs to be called when an [weapon]item is placed into the hand...//onItemUse
            return m_WeaponInUse;
        }
        public void SetCurrentWeapon(Weapon weaponToBeSet)
        {
            //this gets called from Weapon, when you pickup a weapon
            m_WeaponInUse = weaponToBeSet;
        }

        public void OverrideAnimatorController()
        {
            animator = GetComponent<Animator>();
            animator.runtimeAnimatorController = animatorOverrideController;
            animatorOverrideController["DEFAULT_ATTACK"] = m_WeaponInUse.GetAttackAnimationClip();
            //search for the "DEFAULT_ATTACK" and replace it with the weapon in use's animation clip
            //this way weapons can have their own attack Animations
            //prepare better for future with this method (tank, berserk, etc...)
            //future I will be better at animators..blend trees
        }

        /*Use this method for testing auto attack
         */
        public void TestAttackTarget(GameObject target)
        {
            if (!isAttacking)
            {
                Debug.Log("Recvd attack coroutine start request...initiating");
                currentTarget = target;
                StartCoroutine(TestAttack(target, m_WeaponInUse.GetMeleeDamage(), m_WeaponInUse.GetMeleeAttackRadius()));
            }
        }

        /*
        public void AttackTarget(GameObject target)
        {
            currentTarget = target;
            StartCoroutine(AttackTargetRepeatedly());
        }
        IEnumerator AttackTargetRepeatedly()
        {
            // bool canAttack = player.DetermineIfCanAttack();

            float weaponHitPeriod = m_WeaponInUse.GetMinTimeBetweenHits();
            float timeToWait = weaponHitPeriod * m_CustomMovement.GetAnimSpeedMultiplier();

            bool isTimeToHitAgain = Time.time - lastHitTime > timeToWait;


            if (isTimeToHitAgain)
            {
                AttackTargetOnce();
                lastHitTime = Time.time;
            }
            yield return new WaitForSeconds(timeToWait);
        }
        private void AttackTargetOnce()
        {
            //int totalDamageCausedThisHit = player.CalcAdditionalDmgFromPlayerStats(meleeDamage);
            animator.SetTrigger(ATTACK_TRIGGER);
            float timeDelayForAnimationWhereDamageIsActuallyDealt = m_WeaponInUse.GetDamageDelay();
            StartCoroutine(DamageAfterAnimationDelay(timeDelayForAnimationWhereDamageIsActuallyDealt));
        }
        IEnumerator DamageAfterAnimationDelay(float animationWaitOffsetTime)
        {
            yield return new WaitForSecondsRealtime(animationWaitOffsetTime);
            currentTarget.GetComponent<HealthSystem>().TakeDamage();
        }

*/



        private IEnumerator TestAttack(GameObject target, int meleeDamage, float meleeAttackRadius)
        {
            isAttacking = true;
            HealthSystem targetHealth = target.GetComponent<HealthSystem>();
            int totalDamageCausedThisHit = CalculateCriticalDamage(meleeDamage);
            targetHealth.TakeDamage(totalDamageCausedThisHit);
            //Debug.Log("Weapon damage was " + meleeDamage + "...then you maybe critted for total dmg of:" + totalDamageCausedThisHit + " to " + target);
            //Debug.Log("Your target's hp is now " + targetHealth.GetCurrentHP());
                      
            for (int i = 0; i < targetHealth.GetCurrentHP(); i--) //go backwards so they dont hit the same number
            {
                int damageCausedByWeapon = 0;
                if (DetermineIfCanAttack(target, meleeAttackRadius))
                {
                    yield return new WaitForSeconds(m_WeaponInUse.GetMinTimeBetweenHits() + m_WeaponInUse.GetDamageDelay());
                    damageCausedByWeapon = m_WeaponInUse.GetMeleeDamage(); //cache weapons variable dmg, else inconsistant
                    int totalDamageCausedThisNewHit = CalculateCriticalDamage(damageCausedByWeapon);
                    animator.SetTrigger("Attack");
                    targetHealth.TakeDamage(totalDamageCausedThisNewHit);

                    if (totalDamageCausedThisNewHit == 0)
                    {
                        Debug.Log("I missed!");
                    }
                    //Debug.Log("I just did " + totalDamageCausedThisNewHit + "damage to " + target);
                    //Debug.Log("Your target's hp is now " + targetHealth.GetCurrentHP());
                    if (targetHealth.GetCurrentHP() < 1 || target == null)
                    {
                        Debug.Log("Target killed!");
                       // animator.SetTrigger("TargetKilled");
                        //animator.SetBool("CloseEnoughToAttack", false);
                        animator.SetBool("OutOfCombat", false);
                        break;
                    }
                }
                else //we are either not facing the enemy, or not in attack range 
                {
                    Debug.Log("Not facing the enemy or not in range");
                }
                //Debug.Log("iteration number " + i + "enemy hp is " + targetHealth.GetCurrentHP());
            }
            currentTarget = null;
            StopCoroutine("TestAttack");
            Debug.Log("Stopped auto attack coroutine");
        }


        private bool DetermineIfBehindTarget(GameObject target)
        {
            Vector3 toTarget = (target.transform.position - transform.position).normalized;
            bool isBehindEnemy;
            if (Vector3.Dot(toTarget, target.transform.forward) < 0)
            {
                isBehindEnemy = false;
            }
            else
            {
                isBehindEnemy = true; //crit increased, ignore armor
            }
            return isBehindEnemy;
        }
        private int CalculateCriticalDamage(int meleeDamage)
        {
            int additionalDamage = 1; //always needs to be 1 so the crit doesn't take away damage
            bool isCriticalHit = UnityEngine.Random.Range(0f, 1f) <= criticalHitChance;
            //roll between 0.000000 - 1
            //if critChance is 0.1, you've got a 10% chance a number picked randomly between 0 and 1 
            //will be less than or equal to 1
             if (isCriticalHit && DetermineIfBehindTarget(currentTarget))
            {
                Debug.Log("CRITICAL BACKSTAB!");
                float criticalBackstabMultiplier = 0.25f;
                additionalDamage = meleeDamage * Mathf.RoundToInt(criticalBackstabMultiplier + m_WeaponInUse.GetCriticalMultiplier());
                return additionalDamage;
            }
            else if (isCriticalHit)
            {
                Debug.Log("CRITICAL!");
                additionalDamage = meleeDamage * Mathf.RoundToInt(m_WeaponInUse.GetCriticalMultiplier());
                return additionalDamage;
            }

            else
                return meleeDamage;
        }

         
        public bool DetermineIfCanAttack(GameObject target, float meleeAttackRadius)
        {
            if (DetermineIfInMeleeAttackRange(target, meleeAttackRadius) && !DetermineIfFacingTarget(target))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool DetermineIfInMeleeAttackRange(GameObject target, float meleeAttackRadius)
        {
            float distanceToTarget = Vector3.Distance(target.transform.position, this.transform.position);

            //Debug.Log("You need to be " + (distanceToTarget - meleeAttackRadius) + "meters closer");
            return (distanceToTarget <= meleeAttackRadius);


        }
        public bool DetermineIfFacingTarget(GameObject target)
        {
            Vector3 targetDirection = target.transform.position - this.transform.position;
            Vector3 playerforward = this.transform.forward;
            float angle = Vector3.Angle(targetDirection, playerforward);
            //Debug.Log("Angle of direction is " + angle + "You need to be under 60.0 to attack");
            return (angle > 60f);

        }
    }
}