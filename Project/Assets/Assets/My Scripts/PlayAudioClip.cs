﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAudioClip : MonoBehaviour 
{
    AudioSource m_audioSource;
    private void Start()
    {
        m_audioSource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        m_audioSource.Play();
    }

    private void OnTriggerExit(Collider other)
    {
        m_audioSource.Stop();
    }

}
