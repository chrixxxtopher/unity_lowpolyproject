﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/*
 * HealthSystem is responsible for adding and subtracting health from characters.
 * It also updates the HealthBar on the UI. 
 * 
 */
namespace LowPoly.Character //only characters have health
{
    public class HealthSystem : MonoBehaviour
    {
        [SerializeField] Image m_ImageHealthBar;
        [SerializeField] Text m_TextHealthPercentage;
        [SerializeField] AudioClip[] damageSounds;
        [SerializeField] AudioClip[] deathSounds;
        [SerializeField] Animator m_Animator;
        [SerializeField] AudioSource m_AudioSource;
        [SerializeField] int m_MaxHP = 100;
        [SerializeField] int m_CurrentHP = 1;

        const float DEATH_VANISH_SECONDS = 2f;

        private void Start()
        {
            m_CurrentHP = m_MaxHP;
            m_Animator = GetComponent<Animator>(); //animate dying
            m_AudioSource = GetComponent<AudioSource>(); //death & hit noises
            if (this.transform.tag == "Critter" || this.transform.tag =="Attackable")
            {
                m_ImageHealthBar = transform.Find("UI_Socket/EnemyCanvas(Clone)/EnemyHealthBar/GreenForeground").GetComponent<Image>();
                m_TextHealthPercentage = GetComponentInChildren<Text>();
            }
        }

           // m_ImageHealthBar = transform.Find("GreenForeground").GetComponent<Image>();

        //consider Mathf call every update...
        private void UpdateHealthBar()
        {
            //{0} gets rid of the leading 0.....need *100 to work
            m_TextHealthPercentage.text = string.Format("{0} %",Mathf.RoundToInt(HealthAsPercentage * 100));
            m_ImageHealthBar.fillAmount = HealthAsPercentage; //slides the little thingy on inspector
        }

        public float HealthAsPercentage
        {
            get
            {
                return m_CurrentHP / (float)m_MaxHP; //cast bc / is integer division
            }
        }

        public void ReceiveHealing(int amountToHeal)
        {
            Debug.Log("Before HP: " + m_CurrentHP);
            m_CurrentHP = Mathf.RoundToInt(Mathf.Clamp(m_CurrentHP + amountToHeal, 0f, m_MaxHP));
            UpdateHealthBar();
            Debug.Log("After HP:  " + m_CurrentHP);
        }

        public void TakeDamage(int damage)
        {

            bool characterDies = (m_CurrentHP - damage <= 0);
            //clamp the (currentHp - damge) between 0 and maxHp
            m_CurrentHP = (int)Mathf.Clamp(m_CurrentHP - damage, 0, m_MaxHP);
            UpdateHealthBar();
            //var clip = damaSounds[UnityEngine.Random.Range(0, damageSounds.length)];
            //if(damageSounds != null) if no sound then the coroutine doesnt work bc it drops out
                //m_AudioSource.PlayOneShot(damageSounds[0]);
            if (characterDies)
            {
                StartCoroutine(KillCharacter());
            }
        }
        public int GetCurrentHP()
        {
            return m_CurrentHP;
        }
        IEnumerator KillCharacter() // a character is a player is an enemy is a...
        {
            //StopAllCoroutines();
            //play a death sound
            Debug.Log("DEATH SOUND");
            m_Animator.Play("Die");

            var playerComponent = GetComponent<PlayerController>(); //else null
            if (playerComponent && playerComponent.isActiveAndEnabled)
            {
                Debug.Log("dead" + playerComponent);
                //audioSource.clip = deathSounds[Random.Range(0, deathSounds.Length)];
                //audioSource.Play();
                yield return new WaitForSeconds(2f);//audiosource.clip.length instead of 5
                Debug.Log("attempting to load scene");
                SceneManager.LoadScene(0);
            }
            else //it's an enemy (todo fix for npc's)
            {
                //play death sound
                //play death animation
                Destroy(gameObject, DEATH_VANISH_SECONDS);
            }
        }
    }
}