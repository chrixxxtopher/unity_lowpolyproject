﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace LowPoly.Character
{
    public class StaminaSystem : MonoBehaviour
    {
        [SerializeField] CustomMovement m_Character;
        private Vector3 m_Move;
        private bool m_Jump;

        [SerializeField] Image m_ImageStamina;
        [SerializeField] float m_MaxStamina = 100;
        [SerializeField] float m_CurrentStamina = 99;
        private float staminaRegenerationRate = 15f;
        private const int JUMP_COST = 5;
        private const float SPRINT_COST = 0.65f;
        public bool canJump = true;
        public bool canSprint = true; //answer to the player when he tries to jump or sprint


        private void Update()
        {
            if (m_CurrentStamina < m_MaxStamina)
            {
                RegenerateStaminaOverTime();
                UpdateStaminaWheel();
            }
        }

        private void FixedUpdate()
        {
            if (Input.GetKey(KeyCode.LeftShift)) //sprint costs 1stamina per 0.25seconds
            {
                if (AttemptSprint())
                    m_Move *= 2.5f;
                else
                    Debug.Log("Can't sprint, stamina too low");
            }
        }

        void UpdateStaminaWheel()
        {
            m_ImageStamina.fillAmount = StaminaAsPercentage;
        }

        public float StaminaAsPercentage
        {
            get
            {
                return m_CurrentStamina / (float)m_MaxStamina;
            }
        }

        private void RegenerateStaminaOverTime()
        {
            var staminaToAdd = staminaRegenerationRate * Time.deltaTime;
            m_CurrentStamina = Mathf.Clamp(m_CurrentStamina + staminaToAdd, 0, m_MaxStamina); //clamp so it can't go above or below
        }

        private void ConsumeStamina(float amountToConsume)
        {
            float updatedStamina = (m_CurrentStamina - amountToConsume);
            m_CurrentStamina = Mathf.Clamp(updatedStamina, 0, m_MaxStamina); //clamp so it can't go above or below
            UpdateStaminaWheel();
            RegenerateStaminaOverTime();
        }


        //todo: refactor code to put return false first everywhere
        public bool AttemptJump()
        {
            if (JUMP_COST <= m_CurrentStamina)
            {
                ConsumeStamina(JUMP_COST);
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool AttemptSprint()
        {
            if (SPRINT_COST <= m_CurrentStamina)
            {
                ConsumeStamina(SPRINT_COST);
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}