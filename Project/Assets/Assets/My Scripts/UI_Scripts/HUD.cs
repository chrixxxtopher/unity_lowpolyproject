﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    private bool m_IsMessagePanelOpened = false;

    public InventoryList inventory;
    public GameObject MessagePanel;

    void Start()
    {
        //subscribe to events
        inventory.OnItemAdded += Inventory_ItemAdded;
        inventory.OnItemRemoved += Inventory_ItemRemoved;
    }

    private void Inventory_ItemAdded(object sender, InventoryEventArgs e)
    {
        Debug.Log("Received item added event. Updating HUD");
        Transform inventoryPanel = transform.Find("InventoryPanel");
        int index = -1;
        foreach (Transform slot in inventoryPanel)
        {
            index++;

            // Border-> Image, Text.
            Transform imageTransform = slot.GetChild(0).GetChild(0);//item image
            Transform textTransform = slot.GetChild(0).GetChild(1);//item count
            Image image = imageTransform.GetComponent<Image>();
            Text txtCount = textTransform.GetComponent<Text>();

            ItemDragHandler itemDragHandler = imageTransform.GetComponent<ItemDragHandler>();

            //e is item that our inventory secretary told us about
            //each item is stacked in a Slot's Stack and can be referenced by an Id.
            //We can use Count() to see how many things are in that particular Slot's Stack
            if (index == e.m_Item.Slot.Id)
            {
                Debug.Log(index + "e.m_Item.Slot.Id ");
                int itemCount = e.m_Item.Slot.Count;

                image.enabled = true;
                image.sprite = e.m_Item.image;

                if (itemCount > 1)
                    txtCount.text = itemCount.ToString();
                else
                    txtCount.text = "";

                // Store a reference to the item
                itemDragHandler.Item = e.m_Item;

                break;
            }//else go to next transform in inventoryPanel
        }
    }

    private void Inventory_ItemRemoved(object sender, InventoryEventArgs e)
    {
        Transform inventoryPanel = transform.Find("InventoryPanel");
        int index = -1;

        foreach (Transform slot in inventoryPanel)
        {
            index++;

            Transform imageTransform = slot.GetChild(0).GetChild(0);
            Transform textTransform = slot.GetChild(0).GetChild(1);
            Image image = imageTransform.GetComponent<Image>();
            Text txtCount = textTransform.GetComponent<Text>();

            ItemDragHandler itemDragHandler = imageTransform.GetComponent<ItemDragHandler>();

            // We found the item in the UI
            if (itemDragHandler.Item == null)
                continue;

            // Found the slot to remove from
            if (e.m_Item.Slot.Id == index)
            {
                int itemCount = e.m_Item.Slot.Count;
                itemDragHandler.Item = e.m_Item.Slot.FirstItem;

                if (itemCount < 2)
                {
                    txtCount.text = "";
                }
                else
                {
                    txtCount.text = itemCount.ToString();
                }

                if (itemCount == 0)
                {
                    image.enabled = false;
                    image.sprite = null;
                }
                break;
            }
        }
    }






    #region Message Panel Popup Bar 

    public bool IsMessagePanelOpened
    {
        get
        {
            return m_IsMessagePanelOpened;
        }
    }

    public void OpenMessagePanel(InventoryItemBase item)
    {
        MessagePanel.SetActive(true);

        Text mpText = MessagePanel.transform.Find("TextMessage").GetComponent<Text>();
        m_IsMessagePanelOpened = true;
    }

    public void CloseMessagePanel()
    {
        MessagePanel.SetActive(false);

        m_IsMessagePanelOpened = false;
    }
    #endregion Message Panel Popup Bar
}
