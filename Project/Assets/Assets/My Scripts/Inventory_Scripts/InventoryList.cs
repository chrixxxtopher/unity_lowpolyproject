﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This class defines an IList to store unique inventory items. 
 */
public class InventoryList : MonoBehaviour
{
    private const int NUMBER_OF_SLOTS = 10;
    private IList<InventorySlotStack> m_ListOfSlotStacks = new List<InventorySlotStack>();

    /* This defines the inventory events that our player can raise.
     * 
     * Example: 
     * I pick up a Gem. 
     * Immidiately after, I tell my secretary, OnItemAdded, that I just added an Item.
     * She says "Ok, fine - which Item did you pick up, sweety?" 
     * I say, Gem.
     * She says "Ok, I'll let anyone subscribed for ItemAdded updates know!"
     * 
     * Conclusion: 
     * Whenever a Player adds, uses, or removes an item... 
     * he lets his EventHandler know and she will take care of it.
     */
    public event EventHandler<InventoryEventArgs> OnItemAdded;
    public event EventHandler<InventoryEventArgs> OnItemRemoved;
    public event EventHandler<InventoryEventArgs> OnItemUsed;

    //Construct the IList
    public InventoryList()
    {
        //i is the unique ID that references every Stack within the IList of Slots
        for (int i = 0; i < NUMBER_OF_SLOTS; i++)
        {
            //Add an empty Stack to every slot in the inventory.
            m_ListOfSlotStacks.Add(new InventorySlotStack(i));
        }
    }

    private InventorySlotStack FindStackableSlot(InventoryItemBase item)
    {
        //Scan the List to see if and we already have one of these things
        foreach (InventorySlotStack slotStack in m_ListOfSlotStacks)
        {
            if (slotStack.IsStackable(item))
            {
                return slotStack;
            }
        }
        return null;
    }

    private InventorySlotStack FindNextEmptySlot()
    {
        foreach (InventorySlotStack slot in m_ListOfSlotStacks)
        {
            if (slot.IsEmpty)
            {
                return slot;
            }
        }
        return null;
    }

    public void AddItem(InventoryItemBase item)
    {
        Debug.Log("You want to add " + item);

        //make a temporary IList. FindStackableSlot runs 10 times if your NUMBER OF SLOTS is 10.
        InventorySlotStack freeSlot = FindStackableSlot(item);
        if (freeSlot == null)
        {
            Debug.Log("An item you don't currently have has been added ");
            freeSlot = FindNextEmptySlot();
        }
        if (freeSlot != null)
        {
            Debug.Log("Item added");
            freeSlot.AddItem(item);
            Debug.Log("You have " + freeSlot.Count + item + "'s");
            if (OnItemAdded != null)
            {
                Debug.Log("I raised an OnItemAdded event to let your HUD know you added an item");
                OnItemAdded(this, new InventoryEventArgs(item));
            }
        }
    }

    internal void UseItem(InventoryItemBase item)
    {
        if (OnItemUsed != null)
        {
            Debug.Log("I raised an OnItemUsed event to let your player know you used an item");
            OnItemUsed(this, new InventoryEventArgs(item));
        }
    }

    public void RemoveItem(InventoryItemBase item)
    {
        foreach (InventorySlotStack slot in m_ListOfSlotStacks)
        {
            if (slot.Remove(item))
            {
                if (OnItemRemoved != null)
                {
                    //update delgates item has been removed
                    OnItemRemoved(this, new InventoryEventArgs(item));
                }
                break;
            }
        }
    }
}
