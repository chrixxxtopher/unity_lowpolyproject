﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* This is what our secretary, EventHandler, needs to know to inform subscribers.
 * Just tell her the name of the item that is being added, used, or removed.
 * She'll do the rest.
 */
public class InventoryEventArgs : EventArgs
{
    public InventoryItemBase m_Item;

    public InventoryEventArgs(InventoryItemBase item)
    {
        m_Item = item;
    }
}
