﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LowPoly.Weapon;
using LowPoly.Character;

public class EnemyCombat : MonoBehaviour 
{
    [SerializeField] PlayerController player;
    [SerializeField]HUD players_HUD;
    private bool inCombat = false;
    private bool onTheHUD = false;
    public AudioSource m_AudioSource;
    public bool passiveEnemy = true; //yellow is passive, red is active...like WoW
    public bool isAlive = true;
    private bool suspensfulMusicPlaying = false;
    private bool combatAuraPlaying = false;
    public GameObject m_CombatAuraParticleSystem;
    private bool facingAttacker = false;
    [SerializeField] bool initiatedCombatStartup = false;
	void Start () 
    {
        players_HUD = GameObject.Find("HUD").GetComponent<HUD>();
        m_AudioSource = GetComponent<AudioSource>();
	}
	
	void Update () 
    {
		
	}

    //a weapon entered me
    private void OnTriggerEnter(Collider other )
    {
        //todo group into combatstartup method
        if (other.tag == "Weapon" && player.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Attack") && !initiatedCombatStartup)
        {
            if(!suspensfulMusicPlaying)
            {
                PlaySuspensfulMusic();
            }
            if(!onTheHUD)
            {
                ShowMyselfAsTargetOnHUD();
            }
            if(!combatAuraPlaying)
            {
                PlayCombatAuraFX();
            }
            if(!facingAttacker)
            {
                FaceMyAttacker(other.gameObject);
            }
            //take damage
            //start attack coroutine
            initiatedCombatStartup = true;
        }

    }

    private void ShowMyselfAsTargetOnHUD()
    {
        SetMyAvatarAsTheTarget();
        players_HUD.transform.Find("EnemyUIBox").gameObject.SetActive(true);
        onTheHUD = true;
    }

    private void SetMyAvatarAsTheTarget()
    {
        Debug.Log("Placed a " + this + " pic as the player's target");
    }

    private void PlaySuspensfulMusic()
    {
        m_AudioSource.Play();
        suspensfulMusicPlaying = true;
    }

    private void PlayCombatAuraFX()
    {
        var particleObject = Instantiate(m_CombatAuraParticleSystem, transform.position, transform.rotation);
        particleObject.transform.parent = transform;
        particleObject.GetComponentInChildren<ParticleSystem>().Play();
    }

    private void FaceMyAttacker(GameObject other)
    {
        Vector3 direction = other.transform.position - this.transform.position;
        this.transform.forward = direction;
    }

    //private void DestroyParticleFX(GameObject particleObject)
    //{
    //    Destroy(particleObject, 1);
    //}
    //needs a coroutine for attacking

}
