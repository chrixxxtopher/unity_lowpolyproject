﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LowPoly.Character;

public class FirstAid : InventoryItemBase 
{
    [SerializeField] PlayerController playerController;
    [SerializeField] HealthSystem playersHealthSystem;
    [SerializeField] AudioClip healSound;
    [SerializeField] int HealthToRestore = 20;

    private void OnTriggerEnter(Collider other)
    {
        if (this.isActiveAndEnabled && this != null)
        {
            hud.OpenMessagePanel(this);
            playerController.mItemRequestingToBeCollected = this;
        }
    }

    public override void OnUse()
    {
        AudioSource m_AudioSource = GetComponent<AudioSource>();
        //m_AudioSource.pitch = 1.1f;
        m_AudioSource.PlayOneShot(healSound);
        playersHealthSystem.ReceiveHealing(HealthToRestore);
        playerController.inventory.RemoveItem(this);
        Destroy(this.gameObject,1.7f);
    }

    private void OnTriggerExit(Collider other)
    {
        hud.CloseMessagePanel();
    }
}

/* 0.065, 1.2, 0.436
 * -180, 0, 90
 * 205, 196, 200
 */