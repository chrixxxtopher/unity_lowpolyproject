﻿//TODO...REPLACE THIS WITH WEAPON
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using LowPoly.Character;

public class Axe : InventoryItemBase
{
    [SerializeField] PlayerController playerController;

    private void Start()
    {
        playerController = GameObject.FindWithTag("Player").GetComponent<PlayerController>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(this.isActiveAndEnabled && this != null)
        {
            hud.OpenMessagePanel(this);
            playerController.mItemRequestingToBeCollected = this;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        hud.CloseMessagePanel();
    }
}


/*0.4, 0.238, 0.05
 * -167.5, 90, -90
 * 313, 320, 328
 */
