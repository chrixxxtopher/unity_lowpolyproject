﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RuneChanger : MonoBehaviour 
{
    public ParticleSystem ps;
    public Light l;
    public Texture2D[] runes;


    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Entered");
        ps.Play();
        StartCoroutine(ChangeRunes());
    }

    private void OnTriggerExit(Collider other)
    {
        ps.Stop();
        l.cookie = runes[Random.Range(0, runes.Length)];
        StopCoroutine(ChangeRunes());
    }

    IEnumerator ChangeRunes()
    {
        l.cookie = runes[Random.Range(0, runes.Length)];
        yield return new WaitForSeconds(1.5f);
        l.cookie = runes[Random.Range(0, runes.Length)];
    }
}
