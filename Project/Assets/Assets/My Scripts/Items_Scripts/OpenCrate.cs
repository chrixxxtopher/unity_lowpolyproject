﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

public class OpenCrate : MonoBehaviour 
{
	[SerializeField] Animator animator;
    [SerializeField] AudioSource audioSource;
    [SerializeField] AudioClip[] openNoises;
    [SerializeField] AudioClip[] closeNoises;
    bool open = false;

	private void OnTriggerEnter(Collider other) 
	{
            if (!open)
            {
                animator.SetBool("open", true);
                audioSource.PlayOneShot(openNoises[Random.Range(0, openNoises.Length)]);
            }
            else
                animator.SetBool("open", false);
	}

    private void OnTriggerExit(Collider other)
    {
        animator.SetBool("open", false);
        audioSource.PlayOneShot(closeNoises[0]);
    }

}
