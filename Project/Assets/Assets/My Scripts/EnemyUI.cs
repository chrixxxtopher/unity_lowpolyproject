﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LowPoly.Character;

public class EnemyUI : MonoBehaviour 
{
    [SerializeField] GameObject enemyCanvasPrefab;
    Camera cameraToLookAt;
    HealthSystem myHealthSystem;

	void Awake () 
    {
        cameraToLookAt = Camera.main;
        Instantiate(enemyCanvasPrefab, transform.position, Quaternion.identity, transform);
	}


    void LateUpdate () 
    {
        //This makes NPC Health bars always face the player
        transform.LookAt(cameraToLookAt.transform);
	}
}
